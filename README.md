![Screenshot preview of the theme "Determination" by glenthemes](https://64.media.tumblr.com/701631bc7da8391a510d6c4987ab6068/tumblr_pmh1y8zUPM1ubolzro1_1280.png)

**Theme no.:** 06  
**Theme name:** Determination  
**Theme type:** Free / Tumblr use  
**Description:** This theme was previously known as Disintegrate, but has now been reincarnated into Determination. Features an Undertale-inspired pixel heart made from CSS i. in sidebar nav links, ii. on quote posts.  
**Author:** @&hairsp;glenthemes  

**Release date:** [2015-08-29](https://64.media.tumblr.com/71889c217220636e8b2823bb223e2789/tumblr_ntu4d0AlNM1ubolzro1_1280.png)  
**Rework date:** 2019-02-05  
**Last updated:** 2023-05-06

**Post:** [glenthemes.tumblr.com/post/182588716009](https://glenthemes.tumblr.com/post/182588716009)  
**Preview:** [glenthpvs.tumblr.com/determination](https://glenthpvs.tumblr.com/determination)  
**Download:** [pastebin.com/NMPpT4nE](https://pastebin.com/NMPpT4nE)  
