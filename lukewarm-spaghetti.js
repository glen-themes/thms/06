$(document).ready(function(){
    $(".d-min").click(function(){
        $(".desc").hide();
    });
    
    $(".d-max").click(function(){
        $(".desc").show();
    });
    
    $(".d-x").click(function(){
        $(".bigboyy").hide();
    });
    
    //
    
    $(".p-min").click(function(){
        $(this).parents(".posts").find(".pcontent").hide();
    });
    
    $(".p-max").click(function(){
        $(this).parents(".posts").find(".pcontent").show();
    });
    
    $(".p-x").click(function(){
        $(this).parents(".posts").hide();
    });
    
    $(".m-o").click(function(){
        $(".tri-h").toggle();
        $(".ll-h").toggle();
        
        var ding = document.getElementById("melody");
        if (ding.paused) {
            ding.play();
        } else { 
            ding.pause();
        }
    });
    
    var fini = document.getElementById("melody");
    fini.onended = function() {
        $(".tri-h").show();
        $(".ll-h").hide();
    };
    
    $("img").each(function(){
        if($(this).attr("src").indexOf("static.tumblr.com/fd8db8d5cb0f9311af276d4fb6b9fff9/2pnwama/uB6pmgsol/tumblr_static_6rad1tctxs84w4w48444ookws.gif") > -1){
            $(this).wrap("<a title=\'pixel art by pixelins'></a>");
            $(this).css("cursor","help");
        } else if($(this).attr("src").indexOf("static.tumblr.com/2ea4a1fad481c84fc2ea8d9ef2ac743d/2pnwama/m4Lpmgsh7/tumblr_static_8mbbntkkf7k00swwcgccososw.gif") > -1){
            $(this).wrap("<a title='\art by Waterstride-Sunrise - deviantart'></a>");
            $(this).css("cursor","help");
        }
    });
    
});// end ready


// wait for element to be present in the DOM
// credit: Yong Wang: stackoverflow.com/a/61511955/8144506
window.waitForElement = function(selector, interval = 0, maxAttempts = 1500) {
    return new Promise((resolve, reject) => {
        let attempts = 0;

        const v_v = () => {
            const targetElement = document.querySelector(selector);
            if (targetElement) {
                resolve(targetElement);
            } else if (attempts < maxAttempts) {
                attempts++;
                setTimeout(v_v, interval);
            } else {
            }
        };

        v_v();
    });
}

waitForElement(".photo-origin").then(() => {
  let poe = document.querySelectorAll(".photo-origin");
  
  poe ? poe.forEach(poe => {
    let postTypeDis = poe.closest("[post-type='text']").querySelector(".post-type-display");
    if(poe.querySelectorAll("img").length == 1){
      postTypeDis.textContent = "image"
    } else if(poe.querySelectorAll("img").length > 1) {
      postTypeDis.textContent = "photoset"
    }
  }) : ""
})
.catch((error) => {
  console.error(error);
});

waitForElement("[data-npf*='poll']").then(() => {
  let poll = document.querySelectorAll("[data-npf*='poll']");
  
  poll ? poll.forEach(poe => {
    let postTypeDis = poe.closest("[post-type='text']").querySelector(".post-type-display");
    postTypeDis.textContent = "poll"
  }) : ""
})
.catch((error) => {
  console.error(error);
});

waitForElement("[data-npf*='video']").then(() => {
  let vid = document.querySelectorAll("[data-npf*='video']");
  
  vid ? vid.forEach(poe => {
    let postTypeDis = poe.closest("[post-type='text']").querySelector(".post-type-display");
    postTypeDis.textContent = "video"
  }) : ""
})
.catch((error) => {
  console.error(error);
});

waitForElement("figcaption.audio-caption").then(() => {
  let npfAud = document.querySelectorAll("figcaption.audio-caption");
  
  npfAud ? npfAud.forEach(poe => {
    let postTypeDis = poe.closest("[post-type='text']").querySelector(".post-type-display");
    postTypeDis.textContent = "audio"
  }) : ""
})
.catch((error) => {
  console.error(error);
});

waitForElement(".op-blockquote > [data-npf*='audio']").then(() => {
  let extAud1 = document.querySelectorAll(".op-blockquote > [data-npf*='audio']");
  let extAud2 = document.querySelectorAll(".pp > [data-npf*='audio']");
  
  extAud1 ? extAud1.forEach(poe => {
    let postTypeDis = poe.closest("[post-type='text']").querySelector(".post-type-display");
    postTypeDis.textContent = "audio"
  }) : ""
  
  extAud2 ? extAud2.forEach(poe => {
    let postTypeDis = poe.closest("[post-type='text']").querySelector(".post-type-display");
    postTypeDis.textContent = "audio"
  }) : ""
})
.catch((error) => {
  console.error(error);
});
